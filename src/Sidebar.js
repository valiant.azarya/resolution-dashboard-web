import React from "react";
import { useNavigate } from 'react-router-dom';

export function Sidebar() {
  const navigate = useNavigate();

  return (
    <>
      {/*<-- ======= Sidebar ======= -->*/}
      <aside id="sidebar" className="sidebar">

        <ul className="sidebar-nav" id="sidebar-nav">

          <li className="nav-item">
            <a className="nav-link " href="index.html">
              <i className="bi bi-grid"></i>
              <span>Dashboard</span>
            </a>
          </li>{/*<-- End Dashboard Nav -->*/}

          <li className="nav-item">
            <a className="nav-link collapsed" data-bs-target="#lending-nav" data-bs-toggle="collapse" href="#">
              <i className="bi bi-menu-button-wide"></i><span>Lending</span><i className="bi bi-chevron-down ms-auto"></i>
            </a>
            <ul id="lending-nav" className="nav-content collapse " data-bs-parent="#sidebar-nav">
              <li>
                <a href="" onClick={() => {navigate('/lendingpage/case1')}}>
                  <i className="bi bi-circle"></i><span>1. Change Borrower Email</span>
                </a>
              </li>
              <li>
                <a href="" onClick={() => {navigate('/lendingpage/case2')}}>
                  <i className="bi bi-circle"></i><span>2. Manual Choose Offer</span>
                </a>
              </li>
              <li>
                <a href="" onClick={() => {navigate('/lendingpage/case3')}}>
                  <i className="bi bi-circle"></i><span>3. VAT Force Patch</span>
                </a>
              </li>
              <li>
                <a href="" onClick={() => {navigate('/lendingpage/case4')}}>
                  <i className="bi bi-circle"></i><span>4. Loan Ready for TopUp</span>
                </a>
              </li>
              <li>
                <a href="" onClick={() => {navigate('/lendingpage/case5')}}>
                  <i className="bi bi-circle"></i><span>5. Change Disbursement and Due Date</span>
                </a>
              </li>
            </ul>
          </li>{/*<-- End Lending Nav -->*/}

          <li className="nav-item">
            <a className="nav-link collapsed" data-bs-target="#neo-nav" data-bs-toggle="collapse" href="#">
              <i className="bi bi-menu-button-wide"></i><span>NEO</span><i className="bi bi-chevron-down ms-auto"></i>
            </a>
            <ul id="neo-nav" className="nav-content collapse " data-bs-parent="#sidebar-nav">
              <li>
                <a href="forms-elements.html">
                  <i className="bi bi-circle"></i><span>1. UseCase 1</span>
                </a>
              </li>
              <li>
                <a href="forms-layouts.html">
                  <i className="bi bi-circle"></i><span>2. UseCase 2</span>
                </a>
              </li>
            </ul>
          </li>{/*<-- End NEO Nav -->*/}

          <li className="nav-item">
            <a className="nav-link collapsed" data-bs-target="#platform-nav" data-bs-toggle="collapse" href="#">
              <i className="bi bi-menu-button-wide"></i><span>Platform</span><i className="bi bi-chevron-down ms-auto"></i>
            </a>
            <ul id="platform-nav" className="nav-content collapse " data-bs-parent="#sidebar-nav">
              <li>
                <a href="" onClick={() => {navigate('/platformpage/case1')}}>
                  <i className="bi bi-circle"></i><span>1. Unlock Mutation</span>
                </a>
              </li>
              <li>
                <a href="" onClick={() => {navigate('/platformpage/case2')}}>
                  <i className="bi bi-circle"></i><span>2. Rollback Mutation</span>
                </a>
              </li>
            </ul>
          </li>{/*<-- End Platform Nav -->*/}

          <li className="nav-item">
            <a className="nav-link collapsed" data-bs-target="#wealth-nav" data-bs-toggle="collapse" href="#">
              <i className="bi bi-menu-button-wide"></i><span>Wealth</span><i className="bi bi-chevron-down ms-auto"></i>
            </a>
            <ul id="wealth-nav" className="nav-content collapse " data-bs-parent="#sidebar-nav">
              <li>
                <a href="charts-chartjs.html">
                  <i className="bi bi-circle"></i><span>1. UseCase 1</span>
                </a>
              </li>
              <li>
                <a href="charts-apexcharts.html">
                  <i className="bi bi-circle"></i><span>2. UseCase 2</span>
                </a>
              </li>
            </ul>
          </li>{/*<-- End Wealth Nav -->*/}

          <li className="nav-heading">General Use Case</li>

          <li className="nav-item">
            <a className="nav-link collapsed" href="" onClick={() => {navigate('/general')}}>
              <i className="bi bi-card-list"></i>
              <span>General</span>
            </a>
          </li>{/*<-- End Standard Use Case Page Nav -->*/}

        </ul>

        </aside>{/*<-- End Sidebar-->*/}
    </>
  );
}