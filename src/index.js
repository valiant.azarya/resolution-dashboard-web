// import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';

import {
    BrowserRouter,
} from 'react-router-dom';

// CSS
import './assets/vendor/bootstrap/css/bootstrap.min.css';
import './assets/vendor/bootstrap-icons/bootstrap-icons.css';
import './assets/vendor/boxicons/css/boxicons.min.css';
import './assets/vendor/quill/quill.snow.css';
import './assets/vendor/quill/quill.bubble.css';
import './assets/vendor/remixicon/remixicon.css';
import './assets/vendor/simple-datatables/style.css';

// Template Main CSS
import './assets/css/style.css';
import './assets/css/custom.css';

// JS
import './assets/vendor/apexcharts/apexcharts.min.js';
import './assets/vendor/bootstrap/js/bootstrap.bundle.min.js';
import './assets/vendor/chart.js/chart.min.js';
import './assets/vendor/echarts/echarts.min.js';
import './assets/vendor/quill/quill.min.js';
import './assets/vendor/simple-datatables/simple-datatables.js';
import './assets/vendor/tinymce/tinymce.min.js';
import './assets/vendor/php-email-form/validate.js';

// Template Main JS
import './assets/js/main.js';
import { MyRoutes } from './MyRoutes';

const { PUBLIC_URL } = process.env;

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <BrowserRouter basename={PUBLIC_URL}>
        <MyRoutes/>
    </BrowserRouter>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
