import axios from "axios";
import { BASE_URL } from "../../../config/Config";

const testURL = BASE_URL + "/";
const abcURL = "https://httpbin.org/anything";
const localURL = "http://localhost:8080/v1/";

const checkLoanReadyforTopupURL = localURL + "loan-ready-for-topup-check-loan";
const checkLoanIDReadyforTopupURL = localURL + "loan-ready-for-topup-check-loan-id";
const checkChangeDisbursementandDueDateURL = localURL + "change-disbursement-and-due-date-view-date";

const OYValidationURL = BASE_URL + "thirdparty/oy/bank-account-inquiry"
const bankURL = BASE_URL + "assessments/bo/bank-validation"
const getOfferURL = BASE_URL + "koinbisnis/bo/loan/available-offers"
const submitOfferURL = BASE_URL + "koinbisnis/bo/loan/choose-offer"

export const testAPI = () => {
    return axios.get(abcURL);
}

export const getOYValidation = (payload) => {
    return axios.post(OYValidationURL, payload);
}

export const getBankUnderName = (config) => {
    return axios.get(bankURL, config);
}

export const submitBankDisbursement = (payload, config) => {
    return axios.post(bankURL, payload, config);
}

export const getListOffer = (config) => {
    return axios.get(getOfferURL, config);
}

export const submitOffer = (payload, config) => {
    return axios.post(submitOfferURL, payload, config);
}

export const case4CheckLoan = (config) => {
    return axios.post(checkLoanReadyforTopupURL, config);
}

export const case4CheckLoanID = (config) => {
    return axios.post(checkLoanIDReadyforTopupURL, config);
}

export const case5CheckLoan = (config) => {
    return axios.post(checkChangeDisbursementandDueDateURL, config);
}