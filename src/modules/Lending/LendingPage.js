import { React, useState } from "react";
import ReactLoading from 'react-loading';

import { ErrorBoundary } from '../../App';
import { Header } from '../../Header';
import { Sidebar } from '../../Sidebar';
import { Footer } from '../../Footer';

import { BEARERTOKEN } from "../../config/Config";

import { useNavigate, useParams } from 'react-router-dom';
import { getBankUnderName, getListOffer, getOYValidation, submitBankDisbursement, submitOffer, testAPI, case4CheckLoan, case4CheckLoanID, case5CheckLoan } from "./API/LendingPageAPI";
import { ErrorHandler } from "../../helpers/ErrorHandler";
import NavbarCollapse from "react-bootstrap/esm/NavbarCollapse";

export function LendingPage(){

  let { type } = useParams();
  const navigate = useNavigate();

  const [userID, setUserID] = useState();
  const [loanID, setLoanID] = useState();
  const [recipientName, setRecipientName] = useState();

  const [data, setData] = useState({
    type : type || 'notFound',
  });
  
  const [state, setState] = useState({
    requestType: null,
    email: null,
    userId: null,
  });

  const [case1InputList, case1SetInputList] = useState([{oldEmail:"", newEmail:""}]);

  const [case1resp, case1setResp] = useState();

  const [case2resp, case2setResp] = useState();
  const [case2resp2, case2setResp2] = useState();
  const [case2req3, case2setReq3] = useState();
  const [case2resp3, case2setResp3] = useState();
  const [case2resp4, case2setResp4] = useState();
  const [case2req5, case2setReq5] = useState();
  const [case2resp5, case2setResp5] = useState();

  const [case4EmailInput, case4setEmailInput] = useState();

  const [case4resp1, case4setResp1] = useState();
  const [case4resp2, case4setResp2] = useState();
  const [case4resp3, case4setResp3] = useState();
  const [case4resp4, case4setResp4] = useState();

  let case5resp5ctr = 1;
  let case5resp3ctr = 1;

  const [case5resp1, case5setResp1] = useState();
  const [case5resp2, case5setResp2] = useState();
  const [case5resp3, case5setResp3] = useState();
  const [case5resp4, case5setResp4] = useState();
  const [case5resp5, case5setResp5] = useState();


  const [totalFinalDisburse, setTotalFinalDisburse] = useState();
  const [isLoading, setIsLoading] = useState();

  // handle input change
  const handleInputChangeCase1 = (e, index) => {
    const { name, value } = e.target;
    const case1List = [...case1InputList];
    case1List[index][name] = value;
    case1SetInputList(case1List);
  };

  // handle click event of the Remove button
  const handleRemoveClickCase1 = index => {
    const case1List = [...case1InputList];
    case1List.splice(index, 1);
    case1SetInputList(case1List);
  };

  // handle click event of the Add button
  const handleAddClickCase1 = () => {
    case1SetInputList([...case1InputList, { oldEmail: "", newEmail: "" }]);
  };

  const handleSubmitPreCheck1 = e => {
    e.preventDefault();
    console.log(e.target.email.value)
    testAPI()
    .then(res => {
      console.log(res);
      case1setResp(res.data);
    })
    .catch(err => {
      ErrorHandler(err);
    })
  }

  const handleSubmitCase1Resolution = e => {
    e.preventDefault();
    
    testAPI()
    .then(res => {
      console.log(res);
      case1setResp(res.data);
    })
    .catch(err => {
      ErrorHandler(err);
    })
  }

  const handleSubmitCase2RecInfo = e => {
    setIsLoading(true);

    e.preventDefault();
    console.log(e.target.case2_rec_bank.value)
    console.log(e.target.case2_rec_account.value)
    console.log(e.target.case2_rec_name.value)

    setRecipientName(e.target.case2_rec_name.value)

    const payload = {
      "recipient_bank": e.target.case2_rec_bank.value,
      "recipient_account": e.target.case2_rec_account.value,
      "recipient_name": e.target.case2_rec_name.value
    }
    getOYValidation(payload)
    .then(res => {
      console.log(res);
      case2setResp(res.data);
    })
    .catch(err => {
      ErrorHandler(err);
    })
    .finally(() => {
      setIsLoading(false);
    })
  }

  const handleSubmitCase2GetBank = e => {
    setIsLoading(true);
    e.preventDefault();

    setUserID(e.target.case2_userid.value)
    setLoanID(e.target.case2_loanid.value)

    const payload = {
      headers: {
        Authorization: BEARERTOKEN
      },
      params: {
        user_id: e.target.case2_userid.value,
        loan_id: e.target.case2_loanid.value
      }
    }
    getBankUnderName(payload)
    .then(res => {
      console.log(res);
      case2setResp2(res.data);
    })
    .catch(err => {
      ErrorHandler(err);
    })
    .finally(() => {
      setIsLoading(false);
    })
  }

  const handleSubmitCase2SubmitBankDisbursement = e => {
    setIsLoading(true);
    e.preventDefault();

    const config = {
      headers: {
        Authorization: BEARERTOKEN
      },
      params: {
        user_id: userID
      }
    }
    const payload = {
      "bank_name_code": case2resp2.data.code,
      "bank_name": case2resp2.data.name,
      "bank_branch": case2resp2.data.branch,
      "account_number": case2resp.data.oy.recipient_account,
      "account_under_name": case2resp.data.similarity >= 85 ? recipientName : case2resp.data.oy.recipient_name,
      "loan_id": parseInt(loanID),
      "loan_code": e.target.case2_loancode.value,
      "similarity": case2resp.data.similarity,
      "result": case2resp.data.result,
      "ocr_name": case2resp.data.similarity >= 85 ? recipientName : case2resp.data.oy.recipient_name,
      "additional_document": case2resp2.data.additional_document,
      "status": e.target.case2_status.value || "pending"
    }
    submitBankDisbursement(payload, config)
    .then(res => {
      console.log(res);
      case2setReq3(payload);
      case2setResp3(res.data);
    })
    .catch(err => {
      ErrorHandler(err);
    })
    .finally(() => {
      setIsLoading(false);
    })
  }

  const handleSubmitCase2GetOffer = e => {
    setIsLoading(true);
    e.preventDefault();

    const config = {
      headers: {
        Authorization: BEARERTOKEN
      },
      params: {
        user_id: userID,
        loan_id: loanID
      }
    }

    getListOffer(config)
    .then(res => {
      console.log(res);
      case2setResp4(res.data);
      // setTotalFinalDisburse(case2resp4.data[0].amount - case2resp4.data[0].admin_fee - case2resp4.data[0].origination_fee - case2resp4.data[0].insurance_fee - case2resp4.data[0].outstanding);
      // console.log(totalFinalDisburse);
    })
    .catch(err => {
      ErrorHandler(err);
    })
    .finally(() => {
      setIsLoading(false);
    })
  }

  const handleSubmitCase2SubmitOffer = e => {
    setIsLoading(true);
    console.log(case2resp4.data[0].amount - case2resp4.data[0].admin_fee - case2resp4.data[0].origination_fee - case2resp4.data[0].insurance_fee - case2resp4.data[0].outstanding)
    e.preventDefault();

    const config = {
      headers: {
        Authorization: BEARERTOKEN
      }
    }

    const payload = {
      "user_id": parseInt(userID),
      "offer_id": case2resp4.data[0].offer_id,
      "disburse_neo": 0,
      "disburse_bank": (case2resp4.data[0].amount - case2resp4.data[0].admin_fee - case2resp4.data[0].origination_fee - case2resp4.data[0].insurance_fee - case2resp4.data[0].outstanding)
    }

    submitOffer(payload, config)
    .then(res => {
      console.log(res);
      case2setReq5(payload);
      case2setResp5(res.data);
    })
    .catch(err => {
      ErrorHandler(err);
    })
    .finally(() => {
      setIsLoading(false);
    })
  }
  
  const handleSubmitCase3Step1 = e => {
    e.preventDefault();
    console.log(e.target.case3LoanCode.value)

    .then(res => {
      console.log(res);
      case1setResp(res.data);
    })
    .catch(err => {
      ErrorHandler(err);
    })
  }
  
  const handleSubmitCase3Step2 = e => {
    e.preventDefault();
    console.log(e.target.case3LoanCode.value)

    .then(res => {
      console.log(res);
      case1setResp(res.data);
    })
    .catch(err => {
      ErrorHandler(err);
    })
  }
  
  const handleSubmitCase3Step3 = e => {
    e.preventDefault();
    console.log(e.target.case3LoanCode.value)

    .then(res => {
      console.log(res);
      case1setResp(res.data);
    })
    .catch(err => {
      ErrorHandler(err);
    })
  }
  
  const handleSubmitCase3Step4 = e => {
    e.preventDefault();
    console.log(e.target.case3LoanCode.value)

    .then(res => {
      console.log(res);
      case1setResp(res.data);
    })
    .catch(err => {
      ErrorHandler(err);
    })
  }
  
  const handleSubmitCase3Step5 = e => {
    e.preventDefault();
    console.log(e.target.case3LoanCode.value)

    .then(res => {
      console.log(res);
      case1setResp(res.data);
    })
    .catch(err => {
      ErrorHandler(err);
    })
  }

  
  const handleSubmitPreCheckCase4 = e => {
    setIsLoading(true);
    e.preventDefault();

    const payload = {
      "email": e.target.case4Email.value
    }

    case4setEmailInput(e.target.case4Email.value);

    case4CheckLoan(payload)
    .then(res => {
      console.log(res);
      case4setResp1(res.data.data[0].loans);
      // case4setResp2(res.data.data[1]);
      // case4setResp3(res.data.data[2]);
      // case4setResp4(res.data.data[3]);

      // console.log(case4resp2) 
    })
    .catch(err => {
      ErrorHandler(err);
    })
    .finally(() => {
      setIsLoading(false);
    })
  }

  
  const handleSubmitCheckCase4 = e => {
    setIsLoading(true);
    e.preventDefault();

    const payload = {
      "email": case4EmailInput,
      "loan_id": e.target.case4LoanID.value
    }

    console.log(payload);

    case4CheckLoanID(payload)
    .then(res => {
      console.log(res);
      case4setResp2(res.data.data[0]);
      case4setResp3(res.data.data[1]);
      case4setResp4(res.data.data[2]);
    })
    .catch(err => {
      ErrorHandler(err);
    })
    .finally(() => {
      setIsLoading(false);
    })
  }

  
  const handleSubmitPreCheckCase5 = e => {
    setIsLoading(true);
    e.preventDefault();

    const payload = {
      "loan_code": e.target.case5LoanCode.value
    }

    case5CheckLoan(payload)
    .then(res => {
      console.log(res);
      case5setResp1(res.data.data[0]);
      case5setResp2(res.data.data[1]);
      case5setResp3(res.data.data[2]);
      case5setResp4(res.data.data[3]);
      case5setResp5(res.data.data[4]);

      console.log(res.data.data[4])
    })
    .catch(err => {
      ErrorHandler(err);
    })
    .finally(() => {
      setIsLoading(false);
    })
  }

  return (
    <>
      <ErrorBoundary><Header/></ErrorBoundary>
      <ErrorBoundary><Sidebar/></ErrorBoundary>
      <ErrorBoundary>
        <main id="main" className="main">
          { isLoading && 
            <div className="overlay-loading"><ReactLoading type={"spin"} color={"#0d6efd"} height={'10%'} width={'10%'} /></div>
          }
          <div className="pagetitle">
            <h1>POD Lending</h1>
            <nav>
                <ol className="breadcrumb">
                <li className="breadcrumb-item"><a href="" onClick={() => {navigate('/')}}>Dashboard</a></li>
                <li className="breadcrumb-item active">POD Lending</li>
                </ol>
            </nav>
          </div> {/*<!-- End Page Title -->*/}
          

          {data.type == 'notFound' &&
            <section className="section contact">
              <div className="row gy-4">
                <div className="col-xl-12">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="info-box card text-center">
                          <i className="bi bi-card-list"></i>
                          <h3>Case Not Found</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          }

          {data.type == 'case1' &&
            <section className="section contact">
              <div className="row gy-4">
                <div className="col-xl-12">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="info-box card text-center">
                          <h3>Change Borrower Email</h3>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="card">
                  <div className="card-body">
                    <h5 className="card-title">Pre-Check</h5>
                    {case1InputList.map((val, ctr) => {
                      return(
                        <form onSubmit={handleSubmitPreCheck1} action="javascript:void(0)">
                          <div className="row">
                            <div className="col-sm-3">
                              <div className="form-floating mb-3">
                                <input type="email" className="form-control" id="case1OldEmail" name="case1OldEmail" placeholder="Old Email" value={val.oldEmail} onChange={e => handleInputChangeCase1(e, ctr)}/>
                                <label htmlFor="case1OldEmail">Old Email Address</label>
                              </div>
                            </div>
                            <div className="col-sm-3">
                              <div className="form-floating mb-3">
                                <input type="email" className="form-control" id="case1NewEmail" name="case1NewEmail" placeholder="New Email" value={val.newEmail} onChange={e => handleInputChangeCase1(e, ctr)}/>
                                <label htmlFor="case1NewEmail">New Email Address</label>
                              </div>
                            </div>
                            <div className="col-sm-6 my-2">
                              {case1InputList.length !== 1 && <button type="button" style={{ marginRight: '.5rem' }} className="btn btn-danger btn-sm" onClick={() => handleRemoveClickCase1(ctr)}><i className="bi bi-x-lg"></i></button>}
                              { 1 * 0 ? (
                                2 * 0 ? 
                                <button type="button" style={{ marginRight: '.5rem', pointerEvents: 'none' }} className="btn rounded-pill btn-outline-success btn-sm"><i className="bi bi-check-lg"></i> Data Ok</button> :
                                <button type="button" style={{ marginRight: '.5rem', pointerEvents: 'none' }} className="btn rounded-pill btn-outline-danger btn-sm"><i className="bi bi-x-lg"></i> Data Not Ok</button>
                                ) :
                                <button type="button" style={{ marginRight: '.5rem' }} className="btn btn-info">Check Email</button>
                              }
                              { 2 * 0 ?
                                <button type="button" style={{ marginRight: '.5rem', pointerEvents: 'none' }} className="btn rounded-pill btn-outline-success btn-sm"><i className="bi bi-check-lg"></i> Email Updated</button> :
                                <button type="submit" style={{ marginRight: '.5rem' }} className="btn btn-primary">Update Email</button>
                              }
                            </div>
                          </div>
                          <div class="d-grid gap-2 mt-3">
                            {case1InputList.length - 1 == ctr && <button type="button" className="btn btn-success" onClick={handleAddClickCase1}><i className="bi bi-plus-lg"></i> Add More Emails</button>}
                          </div>
                        </form>
                      )
                    })}
                  </div>
                </div>

                <div className="col-xl-12">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="info-box card text-center">
                        <h3>Result</h3>
                        <table className="table datatable">
                          <thead>
                            <tr>
                              <th scope="col">#</th>
                              <th scope="col">Name</th>
                              <th scope="col">Position</th>
                              <th scope="col">Age</th>
                              <th scope="col">Start Date</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th scope="row">1</th>
                              <td>Brandon Jacob</td>
                              <td>Designer</td>
                              <td>28</td>
                              <td>2016-05-25</td>
                            </tr>
                            <tr>
                              <th scope="row">2</th>
                              <td>Bridie Kessler</td>
                              <td>Developer</td>
                              <td>35</td>
                              <td>2014-12-05</td>
                            </tr>
                            <tr>
                              <th scope="row">3</th>
                              <td>Ashleigh Langosh</td>
                              <td>Finance</td>
                              <td>45</td>
                              <td>2011-08-12</td>
                            </tr>
                            <tr>
                              <th scope="row">4</th>
                              <td>Angus Grady</td>
                              <td>HR</td>
                              <td>34</td>
                              <td>2012-06-11</td>
                            </tr>
                            <tr>
                              <th scope="row">5</th>
                              <td>Raheem Lehner</td>
                              <td>Dynamic Division Officer</td>
                              <td>47</td>
                              <td>2011-04-19</td>
                            </tr>
                          </tbody>
                        </table>
                        {/* <p>{JSON.stringify(resp)}</p> */}
                        <div className="row-cols-auto">
                          <button onClick={() => navigator.clipboard.writeText(JSON.stringify(case1resp))} className="btn btn-light"><i className="bi bi-clipboard-check"></i></button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          }

          {data.type == 'case2' && 
            <section className="section contact">
              <div className="row gy-4">
                <div className="col-xl-12">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="info-box card text-center">
                          <h3>Manual Choose Offer</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row gy-4">
                <div className="col-xl-3">
                  {/* <div className="row"> */}
                    <div className="card">
                      <div className="card-body">
                        <h5 className="card-title">1. Input Recepient Information</h5>
                        <form className="row g-3" onSubmit={handleSubmitCase2RecInfo} action="javascript:void(0)">
                          <div className="col-md-12">
                            <div className="form-floating">
                              <input type="text" className="form-control" id="case2_rec_bank" name="case2_rec_bank" />
                              <label htmlFor="case2_rec_bank">Recipient Bank (BCA / BRI)</label>
                            </div>
                          </div>
                          <div className="col-md-12">
                            <div className="form-floating">
                              <input type="text" className="form-control" id="case2_rec_account" name="case2_rec_account" />
                              <label htmlFor="case2_rec_account">Recipient Account Number</label>
                            </div>
                          </div>
                          <div className="col-md-12">
                            <div className="form-floating">
                              <input type="text" className="form-control" id="case2_rec_name" name="case2_rec_name" />
                              <label htmlFor="case2_rec_name">Recipient Account Name</label>
                            </div>
                          </div>
                          <div className="text-center">
                            <button type="submit" className="btn btn-primary">Submit</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  {/* </div> */}
                </div>
                <div className="col-xl-9">
                  <div className="card">
                    <div className="card-body">
                      <h5 className="card-title">Result</h5>
                      <div className="row g-3">
                        <div className="col-md-12">
                          <div className="form-floating">
                            <pre>{JSON.stringify(case2resp, null, 4)}</pre>
                          </div>
                        </div>
                        <div className="text-center">
                          <button type="button" className="btn btn-primary" onClick={() => navigator.clipboard.writeText(JSON.stringify(case2resp, null, 4))}>Copy JSON</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row gy-4">
                <div className="col-xl-3">
                  <div className="row">
                    <div className="card">
                      <div className="card-body">
                        <h5 className="card-title">2. Get Bank Under Name</h5>
                        <form className="row g-3" onSubmit={handleSubmitCase2GetBank} action="javascript:void(0)">
                          <div className="col-md-12">
                            <div className="form-floating">
                              <input type="text" className="form-control" id="case2_userid" name="case2_userid" placeholder="UserID"/>
                              <label htmlFor="case2_userid">User ID</label>
                            </div>
                          </div>
                          <div className="col-md-12">
                            <div className="form-floating">
                              <input type="text" className="form-control" id="case2_loanid" name="case2_loanid" placeholder="LoanID"/>
                              <label htmlFor="case2_loanid">Loan ID</label>
                            </div>
                          </div>
                          <div className="text-center">
                            <button type="submit" className="btn btn-primary">Submit</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-xl-9">
                  <div className="card">
                    <div className="card-body">
                      <h5 className="card-title">Result</h5>
                      <div className="row g-3">
                        <div className="col-md-12">
                          <div className="form-floating">
                            <pre>{JSON.stringify(case2resp2, null, 4)}</pre>
                          </div>
                        </div>
                        <div className="text-center">
                          <button type="button" className="btn btn-primary" onClick={() => navigator.clipboard.writeText(JSON.stringify(case2resp2, null, 4))}>Copy JSON</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row gy-4">
                <div className="col-xl-3">
                  <div className="row">
                    <div className="card">
                      <div className="card-body">
                        <h5 className="card-title">3. Submit Bank Disbursement</h5>
                        <form className="row g-3" onSubmit={handleSubmitCase2SubmitBankDisbursement} action="javascript:void(0)">
                          <div className="col-md-12">
                            <div className="form-floating">
                              <input type="text" className="form-control" id="case2_loancode" name="case2_loancode" placeholder="LoanCode"/>
                              <label htmlFor="case2_loancode">Loan Code</label>
                            </div>
                          </div>
                          <div className="col-md-12">
                            <div className="form-floating">
                              <input type="text" className="form-control" id="case2_status" name="case2_status" placeholder="Status"/>
                              <label htmlFor="case2_status">Status</label>
                            </div>
                          </div>
                          <div className="col-md-12">
                            <div className="form-floating">
                              <input type="text" className="form-control" id="case2_userid" name="case2_userid" placeholder="UserID" value={userID} disabled/>
                              <label htmlFor="case2_userid">User ID</label>
                            </div>
                          </div>
                          <div className="col-md-12">
                            <div className="form-floating">
                              <input type="text" className="form-control" id="case2_loanid" name="case2_loanid" placeholder="LoanID" value={loanID} disabled/>
                              <label htmlFor="case2_loanid">Loan ID</label>
                            </div>
                          </div>
                          <div className="text-center">
                            <button type="submit" className="btn btn-primary">Submit</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-xl-9">
                  <div className="card">
                    <div className="card-body">
                      <h5 className="card-title">Result</h5>
                      <div className="row g-3">
                        <div className="col-md-12">
                          <div className="form-floating">
                            <p>Request Payload</p>
                            <pre>{JSON.stringify(case2req3, null, 4)}</pre>
                          </div>
                        </div>
                        <div className="text-center">
                          <button type="button" className="btn btn-info" onClick={() => navigator.clipboard.writeText(JSON.stringify( case2req3, null, 4))}>Copy JSON</button>
                        </div>
                      </div>
                      <hr/>
                      <div className="row g-3">
                        <div className="col-md-12">
                          <div className="form-floating">
                            <p>Response</p>
                            <pre>{JSON.stringify(case2resp3, null, 4)}</pre>
                          </div>
                        </div>
                        <div className="text-center">
                          <button type="button" className="btn btn-primary" onClick={() => navigator.clipboard.writeText(JSON.stringify( case2resp3, null, 4))}>Copy JSON</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row gy-4">
                <div className="col-xl-3">
                  <div className="row">
                    <div className="card">
                      <div className="card-body">
                        <h5 className="card-title">4. Get Offering Data</h5>
                        <form className="row g-3" onSubmit={handleSubmitCase2GetOffer} action="javascript:void(0)">
                          <div className="col-md-12">
                            <div className="form-floating">
                              <input type="text" className="form-control" id="case2_userid" name="case2_userid" placeholder="UserID" value={userID} disabled/>
                              <label htmlFor="case2_userid">User ID</label>
                            </div>
                          </div>
                          <div className="col-md-12">
                            <div className="form-floating">
                              <input type="text" className="form-control" id="case2_loanid" name="case2_loanid" placeholder="LoanID" value={loanID} disabled/>
                              <label htmlFor="case2_loanid">Loan ID</label>
                            </div>
                          </div>
                          <div className="text-center">
                            <button type="submit" className="btn btn-primary">Submit</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-xl-9">
                  <div className="card">
                    <div className="card-body">
                      <h5 className="card-title">Result</h5>
                      <div className="row g-3">
                        <div className="col-md-12">
                          <div className="form-floating">
                            <pre>{JSON.stringify(case2resp4, null, 4)}</pre>
                          </div>
                        </div>
                        <div className="text-center">
                          <button type="button" className="btn btn-primary" onClick={() => navigator.clipboard.writeText(JSON.stringify(case2resp4, null, 4))}>Copy JSON</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row gy-4">
                <div className="col-xl-3">
                  <div className="row">
                    <div className="card">
                      <div className="card-body">
                        <h5 className="card-title">5. Submit Offer</h5>
                        <form className="row g-3" onSubmit={handleSubmitCase2SubmitOffer} action="javascript:void(0)">
                          <div className="col-md-12">
                            <div className="form-floating">
                              <input type="text" className="form-control" id="case2_userid" name="case2_userid" placeholder="UserID" value={userID} disabled/>
                              <label htmlFor="case2_userid">User ID</label>
                            </div>
                          </div>
                          <div className="text-center">
                            <button type="submit" className="btn btn-primary">Submit</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-xl-9">
                  <div className="card">
                    <div className="card-body">
                      <h5 className="card-title">Result</h5>
                      <div className="row g-3">
                        <div className="col-md-12">
                          <div className="form-floating">
                            <p>Request Payload</p>
                            <pre>{JSON.stringify(case2req5, null, 4)}</pre>
                          </div>
                        </div>
                        <div className="text-center">
                          <button type="button" className="btn btn-info" onClick={() => navigator.clipboard.writeText(JSON.stringify(case2req5, null, 4))}>Copy JSON</button>
                        </div>
                      </div>
                      <hr/>
                      <div className="row g-3">
                        <div className="col-md-12">
                          <div className="form-floating">
                            <p>Response</p>
                            <pre>{JSON.stringify(case2resp5, null, 4)}</pre>
                          </div>
                        </div>
                        <div className="text-center">
                          <button type="button" className="btn btn-primary" onClick={() => navigator.clipboard.writeText(JSON.stringify(case2resp5, null, 4))}>Copy JSON</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          }

          {data.type == 'case3' &&
            <section className="section contact">
              <div className="row gy-4">
                <div className="col-xl-12">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="info-box card text-center">
                          <h3>VAT Force Patch</h3>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="card">
                  <div className="card-body">
                    <h5 className="card-title">1. Check the Loan</h5>
                    <form onSubmit={handleSubmitCase3Step1} action="javascript:void(0)">
                      <div className="row">
                        <div className="col-sm-10">
                          <div className="form-floating mb-3">
                            <input type="email" className="form-control" id="case3LoanCode" name="case3LoanCode" placeholder="Loan Code"/>
                            <label htmlFor="case3LoanCode">Loan Code</label>
                          </div>
                        </div>
                        <div className="col-sm-2 my-2">
                          <button type="submit" className="btn btn-primary">Check Loan Code</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>

                <div className="col-xl-12">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="info-box card text-center">
                        <h3>Result</h3>
                        <table className="table datatable">
                          <thead>
                            <tr>
                              <th scope="col">#</th>
                              <th scope="col">Name</th>
                              <th scope="col">Position</th>
                              <th scope="col">Age</th>
                              <th scope="col">Start Date</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th scope="row">1</th>
                              <td>Brandon Jacob</td>
                              <td>Designer</td>
                              <td>28</td>
                              <td>2016-05-25</td>
                            </tr>
                            <tr>
                              <th scope="row">2</th>
                              <td>Bridie Kessler</td>
                              <td>Developer</td>
                              <td>35</td>
                              <td>2014-12-05</td>
                            </tr>
                            <tr>
                              <th scope="row">3</th>
                              <td>Ashleigh Langosh</td>
                              <td>Finance</td>
                              <td>45</td>
                              <td>2011-08-12</td>
                            </tr>
                            <tr>
                              <th scope="row">4</th>
                              <td>Angus Grady</td>
                              <td>HR</td>
                              <td>34</td>
                              <td>2012-06-11</td>
                            </tr>
                            <tr>
                              <th scope="row">5</th>
                              <td>Raheem Lehner</td>
                              <td>Dynamic Division Officer</td>
                              <td>47</td>
                              <td>2011-04-19</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="card">
                  <div className="card-body">
                    <h5 className="card-title">2. Deactive and Insert new <samp>vat_fee</samp> from <samp>loans.loan_deduction_details</samp></h5>
                    <form onSubmit={handleSubmitCase3Step2} action="javascript:void(0)">
                      <div className="row">
                        <div className="col-sm-10">
                          <div className="form-floating mb-3">
                            <input type="email" className="form-control" id="case3LoanID" name="case3LoanID" placeholder="Loan ID" disabled/>
                            <label htmlFor="case3LoanID">Loan ID</label>
                          </div>
                        </div>
                        <div className="col-sm-2 my-2">
                          <button type="submit" className="btn btn-primary">Execute</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>

                <div className="card">
                  <div className="card-body">
                    <h5 className="card-title">3. Update <samp>deduction_amount</samp> and <samp>disbursement_amount</samp> from <samp>loans.loans</samp></h5>
                    <form onSubmit={handleSubmitCase3Step3} action="javascript:void(0)">
                      <div className="row">
                        <div className="col-sm-10">
                          <div className="form-floating mb-3">
                            <input type="email" className="form-control" id="case3LoanID" name="case3LoanID" placeholder="Loan ID" disabled/>
                            <label htmlFor="case3LoanID">Loan ID</label>
                          </div>
                        </div>
                        <div className="col-sm-2 my-2">
                          <button type="submit" className="btn btn-primary">Execute</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>

                <div className="card">
                  <div className="card-body">
                    <h5 className="card-title">4. Deactive and Insert new <samp>vat_fee_percentage</samp> from <samp>activity.internal_transfer_details</samp></h5>
                    <form onSubmit={handleSubmitCase3Step4} action="javascript:void(0)">
                      <div className="row">
                        <div className="col-sm-10">
                          <div className="form-floating mb-3">
                            <input type="email" className="form-control" id="case3LoanID" name="case3LoanID" placeholder="Loan ID" disabled/>
                            <label htmlFor="case3LoanID">Loan ID</label>
                          </div>
                        </div>
                        <div className="col-sm-2 my-2">
                          <button type="submit" className="btn btn-primary">Execute</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>

                <div className="card">
                  <div className="card-body">
                    <h5 className="card-title">5. Deactive and Insert new <samp>vat_fee</samp> from <samp>activity.cash_in_out_request_details</samp></h5>
                    <form onSubmit={handleSubmitCase3Step5} action="javascript:void(0)">
                      <div className="row">
                        <div className="col-sm-10">
                          <div className="form-floating mb-3">
                            <input type="email" className="form-control" id="case3LoanID" name="case3LoanID" placeholder="Loan ID" disabled/>
                            <label htmlFor="case3LoanID">Loan ID</label>
                          </div>
                        </div>
                        <div className="col-sm-2 my-2">
                          <button type="submit" className="btn btn-primary">Execute</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>

                
              </div>
            </section>
          }

          {data.type == 'case4' &&
            <section className="section contact">
              <div className="row gy-4">
                <div className="col-xl-12">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="info-box card text-center">
                          <h3>Loan Ready for TopUp</h3>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="card">
                  <div className="card-body">
                    <h5 className="card-title">Pre-Check</h5>
                      <form onSubmit={handleSubmitPreCheckCase4} action="javascript:void(0)">
                        <div className="row">
                          <div className="col-sm-4">
                            <div className="form-floating mb-3">
                              <input type="email" className="form-control" id="case4Email" name="case4Email" placeholder="Email"/>
                              <label htmlFor="case4Email">Email Address</label>
                            </div>
                          </div>
                          <div className="col-sm-4 my-2">
                            <button type="submit" style={{ marginRight: '.5rem' }} className="btn btn-info">Check Email</button>
                          </div>
                        </div>
                      </form>
                  </div>
                </div>

                <div className="col-xl-12">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="info-box card text-center">
                        <h3>Loan Information</h3>
                        <form onSubmit={handleSubmitCheckCase4} action="javascript:void(0)">
                          <table className="table datatable">
                            <thead>
                              <tr>
                                <th scope="col">Loan ID</th>
                                <th scope="col">Loan Code</th>
                                <th scope="col">Is Eligible For Top Up</th>
                                <th scope="col">Is Top Up</th>
                                <th scope="col">Loan Status</th>
                                <th scope="col">Applied At</th>
                                <th scope="col">Options</th>
                              </tr>
                            </thead>
                            <tbody>
                              {case4resp1 && Object.values(case4resp1).map(row => {
                                return (
                                  <tr>
                                    {Object.values(row).map(val => {
                                      let res;
                                      function isIsoDate(str) {
                                        if (!/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/.test(str)) return false;
                                        const d = new Date(str); 
                                        return d instanceof Date && !isNaN(d) && d.toISOString()===str; // valid date 
                                      }

                                      function isLowerThanTen(num) {
                                        if (num < 10) {
                                          return String(num).padStart(2, '0');
                                        }
                                        return num;
                                      }

                                      if (isIsoDate(val)) {
                                        let dbDate = new Date(val);
                                        res = isLowerThanTen(dbDate.getFullYear()) + '-' + isLowerThanTen(parseInt(dbDate.getMonth() + 1)) + '-' + isLowerThanTen(dbDate.getDate()) + ' ' +
                                              isLowerThanTen(dbDate.getHours()) + ':' + isLowerThanTen(dbDate.getMinutes()) +  ':' + isLowerThanTen(dbDate.getSeconds()) + ':' + isLowerThanTen(dbDate.getMilliseconds());
                                      }
                                      else {
                                        res = val;
                                      }
                                      return(
                                        <td>{res}</td>
                                      )
                                    })}
                                    <td style={{display: "none"}}><input type="text" id="case4LoanID" name="case4LoanID" value={row.loan_id}/></td>
                                    <td><button type="submit" style={{ marginRight: '.5rem' }} className="btn btn-info">Check</button></td>
                                  </tr>
                                )
                              })}
                            </tbody>
                          </table>
                        </form>
                      </div>
                      <div className="info-box card text-center">
                        <h3>Loan Details</h3>
                        <div style={{ whiteSpace: 'nowrap', position: 'relative', overflowX: 'scroll', overflowY: 'hidden' }}>
                          <table className="table datatable">
                            <thead>
                              <tr>
                                {case4resp2 && Object.keys(case4resp2.loan_detail).map(header => {
                                  let str = header;
                                  str = str.replaceAll('_', ' ');

                                  function capitalize(str) {
                                    return str.charAt(0).toUpperCase() + str.slice(1);
                                  }

                                  const head = str.split(' ').map(capitalize).join(' ');
                                  return(
                                    <th scope="col">{head}</th>
                                  )
                                })}
                              </tr>
                            </thead>
                            <tbody>
                              {case4resp2 && Object.values(case4resp2).map(row => {
                                return (
                                  <tr>
                                    {Object.values(row).map(val => {
                                      return(
                                        <td>{val}</td>
                                      )
                                    })}
                                  </tr>
                                )
                              })}
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div className="info-box card text-center">
                        <h3>Loan Schedules</h3>
                        <table className="table datatable">
                          <thead>
                            <tr>
                              <th scope="col">Installment No</th>
                              <th scope="col">Due Date</th>
                              <th scope="col">Installment Status</th>
                            </tr>
                          </thead>
                          <tbody>
                            {case4resp3 && Object.values(case4resp3.loan_schedules).map(row => {
                              return (
                                <tr>
                                  {Object.values(row).map(val => {
                                    return(
                                      <td>{val}</td>
                                    )
                                  })}
                                </tr>
                              )
                            })}
                          </tbody>
                        </table>
                      </div>
                      <div className="info-box card text-center">
                        <h3>Rejected Loan</h3>
                          <table className="table datatable">
                            <thead>
                              <tr>
                                <th scope="col">Top Up Loan Request Detail ID</th>
                                <th scope="col">Top Up Loan Request ID</th>
                                <th scope="col">Loan ID</th>
                                <th scope="col">Top Up Request Status</th>
                              </tr>
                            </thead>
                            <tbody>
                              {case4resp4 && Object.values(case4resp4.rejected_loan).map(row => {
                                return (
                                  <tr>
                                    {Object.values(row).map(val => {
                                      return(
                                        <td>{val}</td>
                                      )
                                    })}
                                  </tr>
                                )
                              })}
                            </tbody>
                          </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          }

          {data.type == 'case5' &&
            <section className="section contact">
              <div className="row gy-4">
                <div className="col-xl-12">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="info-box card text-center">
                          <h3>Change Disbursement Date and Due Date</h3>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="card">
                  <div className="card-body">
                    <h5 className="card-title">Pre-Check</h5>
                      <form onSubmit={handleSubmitPreCheckCase5} action="javascript:void(0)">
                        <div className="row">
                          <div className="col-sm-4">
                            <div className="form-floating mb-3">
                              <input type="text" className="form-control" id="case5LoanCode" name="case5LoanCode" placeholder="Loan Code"/>
                              <label htmlFor="case5LoanCode">Loan Code</label>
                            </div>
                          </div>
                          <div className="col-sm-4 my-2">
                            <button type="submit" style={{ marginRight: '.5rem' }} className="btn btn-info">Check Loan Code</button>
                          </div>
                        </div>
                      </form>
                  </div>
                </div>

                <div className="col-xl-12">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="info-box card text-center">
                        <h3>Loan Disbursement Date Information</h3>
                        <div style={{ whiteSpace: 'nowrap', position: 'relative', overflowX: 'scroll', overflowY: 'hidden' }}>
                          <table className="table datatable">
                            <thead>
                              <tr>
                                <th scope="col">Loan ID</th>
                                <th scope="col">Loan Code</th>
                                <th scope="col">Loan Status</th>
                                <th scope="col">Transfer Date</th>
                                <th scope="col">System Transfer Date</th>
                                <th scope="col">Paid</th>
                              </tr>
                            </thead>
                            <tbody>
                              {case5resp1 && Object.values(case5resp1).map(row => {
                                return (
                                  <tr>
                                    {Object.values(row).map(val => {
                                      let res;
                                      function isIsoDate(str) {
                                        if (!/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/.test(str)) return false;
                                        const d = new Date(str); 
                                        return d instanceof Date && !isNaN(d) && d.toISOString()===str; // valid date 
                                      }

                                      function isLowerThanTen(num) {
                                        if (num < 10) {
                                          return String(num).padStart(2, '0');
                                        }
                                        return num;
                                      }

                                      if (isIsoDate(val)) {
                                        let dbDate = new Date(val);
                                        res = isLowerThanTen(dbDate.getFullYear()) + '-' + isLowerThanTen(parseInt(dbDate.getMonth() + 1)) + '-' + isLowerThanTen(dbDate.getDate()) + ' ' +
                                              isLowerThanTen(dbDate.getHours()) + ':' + isLowerThanTen(dbDate.getMinutes()) +  ':' + isLowerThanTen(dbDate.getSeconds()) + ':' + isLowerThanTen(dbDate.getMilliseconds());
                                      }
                                      else {
                                        res = val;
                                      }
                                      return(
                                        <td>{res}</td>
                                      )
                                    })}
                                  </tr>
                                )
                              })}
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div className="info-box card text-center">
                        <h3>Loan Repayment Schedules</h3>
                        <div style={{ whiteSpace: 'nowrap', position: 'relative', overflowX: 'hidden', overflowY: 'scroll', maxHeight: 300 }}>
                          <table className="table datatable">
                            <thead>
                              <tr>
                                <th scope="col">Installment No</th>
                                <th scope="col">Schedule ID</th>
                                <th scope="col">Installment Status</th>
                                <th scope="col">Due Date</th>
                                <th scope="col">Late Collectible</th>
                                <th scope="col">Late Spreadable</th>
                              </tr>
                            </thead>
                            <tbody>
                              {case5resp2 && Object.values(case5resp2.loan_repayment_schedule).map(row => {
                                return (
                                  <tr>
                                    {Object.values(row).map(val => {
                                      return(
                                        <td>{val}</td>
                                      )
                                    })}
                                  </tr>
                                )
                              })}
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div className="info-box card text-center">
                        <h3>Loan Lender Schedules</h3>
                        <div style={{ whiteSpace: 'nowrap', position: 'relative', overflowX: 'hidden', overflowY: 'scroll', maxHeight: 300 }}>
                          <table className="table datatable">
                            <thead>
                              <tr>
                                <th scope="col">No</th>
                                <th scope="col">Installment No</th>
                                <th scope="col">Lender Schedule ID</th>
                                <th scope="col">Late Return </th>
                                <th scope="col">Status Payment</th>
                                <th scope="col">Due Date</th>
                              </tr>
                            </thead>
                            <tbody>
                              {case5resp3 && Object.values(case5resp3.loan_lender_schedule).map(row => {
                                return (
                                  <tr>
                                    <td>{case5resp3ctr++}</td>
                                    {Object.values(row).map(val => {
                                      return(
                                        <td>{val}</td>
                                      )
                                    })}
                                  </tr>
                                )
                              })}
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div className="info-box card text-center">
                        <h3>Activtiy Disbursement and Due Date</h3>
                        <table className="table datatable">
                          <thead>
                            <tr>
                              <th scope="col">Detail</th>
                              <th scope="col">Value</th>
                            </tr>
                          </thead>
                          <tbody>
                            {case5resp4 && Object.values(case5resp4.activity_disbursement_date_and_due_date).map(row => {
                              return (
                                <tr>
                                  {Object.values(row).map(val => {
                                    return(
                                      <td>{val}</td>
                                    )
                                  })}
                                </tr>
                              )
                            })}
                          </tbody>
                        </table>
                      </div>
                      <div className="info-box card text-center">
                        <h3>Portfolio Repayment Schedule</h3>
                        <div style={{ whiteSpace: 'nowrap', position: 'relative', overflowX: 'hidden', overflowY: 'scroll', maxHeight: 500 }}>
                          <table className="table datatable">
                            <thead>
                              <tr>
                                <th scope="col">No</th>
                                {case5resp5 && Object.keys(case5resp5.portfolio_repayment_schedule[0]).map(header => {
                                  let str = header;
                                  str = str.replaceAll('_', ' ');

                                  function capitalize(str) {
                                    return str.charAt(0).toUpperCase() + str.slice(1);
                                  }

                                  const head = str.split(' ').map(capitalize).join(' ');
                                  return(
                                    <th scope="col">{head}</th>
                                  )
                                })}
                              </tr>
                            </thead>
                            <tbody>
                              {case5resp5 && Object.values(case5resp5.portfolio_repayment_schedule).map(row => {
                                return (
                                  <tr>
                                    <td>{case5resp5ctr++}</td>
                                    {Object.values(row).map(val => {
                                      let res;
                                      function isIsoDate(str) {
                                        if (!/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/.test(str)) return false;
                                        const d = new Date(str); 
                                        return d instanceof Date && !isNaN(d) && d.toISOString()===str; // valid date 
                                      }

                                      function isLowerThanTen(num) {
                                        if (num < 10) {
                                          return String(num).padStart(2, '0');
                                        }
                                        return num;
                                      }

                                      if (isIsoDate(val)) {
                                        let dbDate = new Date(val);
                                        res = isLowerThanTen(dbDate.getFullYear()) + '-' + isLowerThanTen(parseInt(dbDate.getMonth() + 1)) + '-' + isLowerThanTen(dbDate.getDate()) + ' ' +
                                              isLowerThanTen(dbDate.getHours()) + ':' + isLowerThanTen(dbDate.getMinutes()) +  ':' + isLowerThanTen(dbDate.getSeconds()) + ':' + isLowerThanTen(dbDate.getMilliseconds());
                                      }
                                      else {
                                        res = val;
                                      }
                                      return(
                                        <td>{res}</td>
                                      )
                                    })}
                                  </tr>
                                )
                              })}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          }

        </main> {/*<!-- End #main -->*/}
      </ErrorBoundary>
      <ErrorBoundary><Footer/></ErrorBoundary>
    </>
  )
}