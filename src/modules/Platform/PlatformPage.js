import { React, useState } from "react";
import ReactLoading from 'react-loading';

import { ErrorBoundary } from '../../App';
import { Header } from '../../Header';
import { Sidebar } from '../../Sidebar';
import { Footer } from '../../Footer';

import { BEARERTOKEN } from "../../config/Config";

import { useNavigate, useParams } from 'react-router-dom';
import { unlockMutation, rollbackMutation } from "./API/PlatformPageAPI";
import { ErrorHandler } from "../../helpers/ErrorHandler";

export function PlatformPage(){
  
  let { type } = useParams();
  const navigate = useNavigate();

  const [data, setData] = useState({
    type : type || 'notFound',
  });
  
  const [isLoading, setIsLoading] = useState();

  const [case1req, case1setReq] = useState();
  const [case1resp, case1setResp] = useState();

  const [case2req, case2setReq] = useState();
  const [case2resp, case2setResp] = useState();

  const handleSubmitCase1UnlockMutation = e => {
    setIsLoading(true);
    e.preventDefault();

    console.log(e.target.case1_mutation_id.value);

    const config = {
      headers: {
        Authorization: BEARERTOKEN,
        "Content-Type": "application/JSON"
      }
    }
    unlockMutation(e.target.case1_mutation_id.value, config)
    .then(res => {
      console.log(res);
      case1setReq(payload);
      case1setResp(res.data);
    })
    .catch(err => {
      ErrorHandler(err);
    })
    .finally(() => {
      setIsLoading(false);
    })
  }

  const handleSubmitCase2RollbackMutation = e => {
    setIsLoading(true);
    e.preventDefault();

    const config = {
      headers: {
        Authorization: BEARERTOKEN,
        "Content-Type": "application/JSON"
      }
    }
    const payload = {
      "isRollback": true,
      "rollbackMutation": {
        "type": "037d3b11-01f3-11ea-b437-00163e016d4c",         // --> OrderPaymentRefund
        "accountType": "809d8e5e-fb90-11e9-b437-00163e016d4c",  // --> Available
        "referenceCode": e.target.case2_reference_code.value,    // <REFERENCE-CODE>
        "impactedType": "04020691-f569-11e9-97fa-00163e010bca", // --> Order
        "impactedTo": e.target.case2_order_id.value,             // <ORDER-ID>
        "descriptionEN": e.target.case2_order_code_en.value,      // <ORDER-CODE>
        "descriptionID": e.target.case2_order_code_id.value       // <ORDER-CODE>
      }
    }
    rollbackMutation(e.target.case2_mutation_id.value, payload, config)
    .then(res => {
      console.log(res);
      case2setReq(payload);
      case2setResp(res.data);
    })
    .catch(err => {
      ErrorHandler(err);
    })
    .finally(() => {
      setIsLoading(false);
    })
  }

  return (
    <>
      <ErrorBoundary><Header/></ErrorBoundary>
      <ErrorBoundary><Sidebar/></ErrorBoundary>
      <ErrorBoundary>
        <main id="main" className="main">
          { isLoading && 
            <div className="overlay-loading"><ReactLoading type={"spin"} color={"#0d6efd"} height={'10%'} width={'10%'} /></div>
          }
          <div className="pagetitle">
            <h1>POD Platform</h1>
            <nav>
                <ol className="breadcrumb">
                <li className="breadcrumb-item"><a href="" onClick={() => {navigate('/')}}>Dashboard</a></li>
                <li className="breadcrumb-item active">POD Platform</li>
                </ol>
            </nav>
          </div> {/*<!-- End Page Title -->*/}

          {data.type == 'notFound' &&
            <section className="section contact">
              <div className="row gy-4">
                <div className="col-xl-12">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="info-box card text-center">
                          <i className="bi bi-card-list"></i>
                          <h3>Case Not Found</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          }

          {data.type == 'case1' && 
            <section className="section contact">
              <div className="row gy-4">
                <div className="col-xl-12">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="info-box card text-center">
                          <h3>Unlock Mutation</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row gy-4">
                <div className="col-xl-3">
                  <div className="card">
                    <div className="card-body">
                      <h5 className="card-title">Unlock Mutation</h5>
                      <form className="row g-3" onSubmit={handleSubmitCase1UnlockMutation} action="javascript:void(0)">
                        <div className="col-md-12">
                          <div className="form-floating">
                            <input type="text" className="form-control" id="case1_mutation_id" name="case1_mutation_id" />
                            <label htmlFor="case1_mutation_id">Mutation ID</label>
                          </div>
                        </div>
                        <div className="text-center">
                          <button type="submit" className="btn btn-primary">Submit</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                <div className="col-xl-9">
                  <div className="card">
                    <div className="card-body">
                      <h5 className="card-title">Result</h5>
                      <div className="row g-3">
                        <div className="col-md-12">
                          <div className="form-floating">
                            <pre>{JSON.stringify(case1resp, null, 4)}</pre>
                          </div>
                        </div>
                        <div className="text-center">
                          <button type="button" className="btn btn-primary" onClick={() => navigator.clipboard.writeText(JSON.stringify(case1resp, null, 4))}>Copy JSON</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          }
          {data.type == 'case2' && 
            <section className="section contact">
              <div className="row gy-4">
                <div className="col-xl-12">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="info-box card text-center">
                          <h3>Rollback Mutation</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row gy-4">
                <div className="col-xl-3">
                  <div className="row">
                    <div className="card">
                      <div className="card-body">
                        <h5 className="card-title">Rollback Mutation</h5>
                        <form className="row g-3" onSubmit={handleSubmitCase2RollbackMutation} action="javascript:void(0)">
                          <div className="col-md-12">
                            <div className="form-floating">
                              <input type="text" className="form-control" id="case2_mutation_id" name="case2_mutation_id" placeholder="Mutation ID"/>
                              <label htmlFor="case2_mutation_id">Mutation ID</label>
                            </div>
                          </div>
                          <div className="col-md-12">
                            <div className="form-floating">
                              <input type="text" className="form-control" id="case2_order_id" name="case2_order_id" placeholder="Order ID"/>
                              <label htmlFor="case2_order_id">Order ID</label>
                            </div>
                          </div>
                          <div className="col-md-12">
                            <div className="form-floating">
                              <input type="text" className="form-control" id="case2_reference_code" name="case2_reference_code" placeholder="Reference Code"/>
                              <label htmlFor="case2_reference_code">Reference Code</label>
                            </div>
                          </div>
                          <div className="col-md-12">
                            <div className="form-floating">
                              <input type="text" className="form-control" id="case2_order_code_en" name="case2_order_code_en" placeholder="Order Code EN"/>
                              <label htmlFor="case2_order_code_en">Order Code / Description (EN)</label>
                            </div>
                          </div>
                          <div className="col-md-12">
                            <div className="form-floating">
                              <input type="text" className="form-control" id="case2_order_code_id" name="case2_order_code_id" placeholder="Order Code ID"/>
                              <label htmlFor="case2_order_code_id">Order Code / Description (ID)</label>
                            </div>
                          </div>
                          <div className="text-center">
                            <button type="submit" className="btn btn-primary">Submit</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-xl-9">
                  <div className="card">
                    <div className="card-body">
                      <h5 className="card-title">Result</h5>
                      <div className="row g-3">
                        <div className="col-md-12">
                          <div className="form-floating">
                            <pre>{JSON.stringify(case2resp, null, 4)}</pre>
                          </div>
                        </div>
                        <div className="text-center">
                          <button type="button" className="btn btn-primary" onClick={() => navigator.clipboard.writeText(JSON.stringify(case2resp, null, 4))}>Copy JSON</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          }
        </main>
      </ErrorBoundary>
      <ErrorBoundary><Footer/></ErrorBoundary>
    </>
  )
}