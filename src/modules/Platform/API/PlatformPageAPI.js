import axios from "axios";
import { BASE_URL } from "../../../config/Config"; //-->https://asgard.koinworks.com/v1/

const UnlockMutationURL = BASE_URL + "activities/mutations/";

export const unlockMutation = (mutationID, config) => {
  let completeUnlockMutationURL = UnlockMutationURL + mutationID + "/unlock";
  return axios.post(completeUnlockMutationURL, config);
}
export const rollbackMutation = (mutationID, payload, config) => {
  let completeUnlockMutationURL = UnlockMutationURL + mutationID + "/unlock";
  return axios.post(completeUnlockMutationURL, payload, config);
}
