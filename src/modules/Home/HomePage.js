import { act } from "@testing-library/react";
import axios from "axios";
import React from "react";
import { Table } from "react-bootstrap";

export function HomePage(props){
    const [nama, setNama] = React.useState("Initial Name");
    const [products, setProducts] = React.useState([]);
    const [detailCustomer, setDetailCustomer] = React.useState({
        email: null,
        userId: null,
    });

    function getProducts(){
        axios.get("https://dummyjson.com/products")
        .then(res => {
            console.log(res.data);
            setProducts(res.data.products);

        })
        .catch(err =>  {
            console.log(err);
        })
    }

    function getUserbyEmail(email){
        const payload = {
            email: email
        };
        console.log(payload)
        axios.post("http://localhost:8080/test", payload)
        .then(res => {
            const data = res.data;
            console.log(data);
            setProducts(data);
            setDetailCustomer({
                ...detailCustomer,
                email: email,
            })
        })
        .catch(err =>  {
            console.log(err);
        })
    }

    function getUserBusinessProfilebyUserID(userid){
        const payload ={
            userid: detailCustomer.userId,
        }
        // setelah melalui post nanti dapet user id, lalu user idnya ditampung
        // setDetailCustomer({
        //     ...detailCustomer,
        //     userId: "USER_01"
        // })
    }

    return (
        <div className="container">
            <div className="row">
                <div className="col-6 d-flex justify-content-center">
                    Hello
                </div>
                <div className="col-6 d-flex justify-content-center">
                    World {nama}
                </div>
            </div>
            <div className="row">
            <form>
                <label>
                    Email:
                    <input type="text" name="email" />
                </label>
                <input type="submit" value="Submit" />
            </form>
            </div>
            <button className="btn btn-primary" onClick={() => getProducts()}>Get Products</button>
            <button className="btn btn-primary" onClick={() => getUserbyEmail('hhendiana7@gmail.com')}>Get User</button>
            <Table>
                <thead>
                    <tr>
                        {products.length > 0 && Object.keys(products[0]).map(header => {
                            return(
                                <th>{header}</th>
                            )
                        })}
                    </tr>
                </thead>
                <tbody>
                    {products.map(row => {
                        return (
                            <tr>
                                {Object.values(row).map(val => {
                                    return(
                                        <td>{val}</td>
                                    )
                                })}
                            </tr>
                        )
                    })}
                </tbody>
            </Table>
            {detailCustomer.email && (
                <button onClick={() => {getUserBusinessProfilebyUserID(detailCustomer.email)}}>Ini button baru muncul kalo udah ada user email</button>
            )}
            {detailCustomer.userId && (
                <button>Ini button baru muncul kalo udah ada user id</button>
            )}
        </div>
    )
}