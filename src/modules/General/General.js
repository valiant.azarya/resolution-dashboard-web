import { React, useState } from "react";

import { ErrorBoundary } from '../../App';
import { Header } from '../../Header';
import { Sidebar } from '../../Sidebar';
import { Footer } from '../../Footer';

import { useNavigate } from 'react-router-dom';

export function General() {

  const navigate = useNavigate();

  const [table, setTabel] = useState({
    type : 'users',
  });

  return (
    <>
      <ErrorBoundary><Header/></ErrorBoundary>
      <ErrorBoundary><Sidebar/></ErrorBoundary>
      <ErrorBoundary>
        <main id="main" className="main">
          <div className="pagetitle">
            <h1>General</h1>
            <nav>
                <ol className="breadcrumb">
                <li className="breadcrumb-item"><a href="" onClick={() => {navigate('/')}}>Dashboard</a></li>
                <li className="breadcrumb-item active">General</li>
                </ol>
            </nav>
          </div> {/*<!-- End Page Title -->*/}

          {table.type == 'users' &&
            <section className="section contact">
              <div className="row gy-4">
                <div className="col-xl-3">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="info-box card text-center">
                          <i className="bi bi-card-list"></i>
                          <h3>Users</h3>
                          <p>BO 1.0 & BO 2.0</p>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-xl-9">
                  <div className="card p-4">
                    <form action="forms/contact.php" method="post" className="php-email-form">
                      <div className="row gy-4">
                        <div className="col-md-4">
                          <input type="email" className="form-control" name="email" placeholder="Email" required/>
                        </div>
                        <div className="col-md-4">
                          <input type="email" className="form-control" name="userid" placeholder="User ID" required/>
                        </div>

                        <div className="col-md-4 text-center">
                          <button type="submit">Check Users</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </section>
          }

          {table.type == 'loans' &&
            <section className="section contact">
              <div className="row gy-4">
                <div className="col-xl-3">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="info-box card text-center">
                          <i className="bi bi-card-list"></i>
                          <h3>Loans</h3>
                          <p>BO 1.0 & BO 2.0</p>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-xl-9">
                  <div className="card p-4">
                    <form action="forms/contact.php" method="post" className="php-email-form">
                      <div className="row gy-4">
                        <div className="col-md-4">
                          <input type="text" className="form-control" name="loancode" placeholder="Loan Code"/>
                        </div>
                        <div className="col-md-4">
                          <input type="text" className="form-control" name="loanid" placeholder="Loan ID"/>
                        </div>

                        <div className="col-md-4 text-center">
                          <button type="submit">Check Loans</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </section>
            
          }
        </main> {/*<!-- End #main -->*/}
      </ErrorBoundary>
      <ErrorBoundary><Footer/></ErrorBoundary>
    </>
    );
}