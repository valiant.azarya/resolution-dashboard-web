import React from "react";
import { Router, Route, Routes } from "react-router-dom";
import App from "./App";
import { General } from "./modules/General/General";
import { LendingPage } from "./modules/Lending/LendingPage";
import { PlatformPage } from "./modules/Platform/PlatformPage";

export function MyRoutes(){
    return (
        <Routes>
            <Route exact path="/" element={<App/>}/>
            <Route exact path="/app" element={<App/>}/>
            <Route exact path="/general" element={<General/>}/>
            <Route exact path="/lendingpage" element={<LendingPage/>}/>
            <Route exact path="/lendingpage/:type" element={<LendingPage/>}/>
            <Route exact path="/platformpage" element={<PlatformPage/>}/>
            <Route exact path="/platformpage/:type" element={<PlatformPage/>}/>
            {/* <Route exact path="/podlending/:id" element={<LendingPage globalStore={globalStore} {...props}/>} render={(props) => <LendingPage globalStore={globalStore} {...props} /> } /> */}
        </Routes>
    )
}